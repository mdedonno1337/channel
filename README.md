# Resources

This repository contains supporting resources for my [YouTube Channel](https://adrian.goins.tv). Each video or video series will have its own folder, and any scripts, manifests, or configuration files that I use in the tutorial will be included here.

## Videos

- [E01](resources/E01) - Rancher 101: How to Use RKE to Make Rancher HA
- [E02](resources/E02) - Rancher 101: How to Install Rancher on a Single Node
- [E03](resources/E03) - Rancher 101: How To Make Your Rancher Server Immortal
- [E04](resources/E04) - Rancher 101: Rancher Backup to Minio
- [E05](resources/E05) - Rancher Standalone HA Disaster
- [E06](resources/E06) - Rancher 101: How to Launch an EKS Cluster with Rancher
- [E07](resources/E07) - Is LetsEncrypt Going to Shut You Down?
- [E08](resources/E08) - Kubernetes 101: Shortcuts to Make the K8s CLI Less Nasty
- [E09](resources/E09) - Tanzu Mission Control-C Rancher?
- [E10](resources/E10) - How Long Does Rancher Really Take?
- [E11](resources/E11) - Kubernetes 101: Why You Need To Use MetalLB
- [E12](resources/E12) - Your Containership.io Lifeboat is Here.
- [E13](resources/E13) - Kubernetes 101: Can You Run NFS In Kubernetes?
- [E14](resources/E14) - Be Your Own DNS-Over-HTTPS Provider - Part One
- [E15](resources/E15) - Be Your Own DNS-Over-HTTPS Provider - Part Two
- [E16](resources/E16) - Open Source Enterprise Storage for K8s!
- [E17](resources/E17) - Disaster Recovery Storage for K8s With Longhorn
- [E18](resources/E18) - Kubernetes 101: Get Better Uptime with K8s Health Checks
- [E19](resources/E19) - Rancher 101: Custom and Imported Kubernetes Clusters
- [E20](resources/E20) - Seven Days With Rio (7DWR)
- [E21](resources/E21) - Using MetalLB with Nginx and Rancher
